using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerScript : MonoBehaviour
{
	
	// 플레이어 기본 변수
	
	public float _speed; // 플레이어 이동속도

    public int maxHP = 3; // 플레이어 최대 체력
	public int _hp; // 플레이어 현재 체력
	
    public Animator _rabbit; // 플레이어 애니메이터

	
	public bool _gameWin; // 승리 체크
    public bool _playerLive = true; // 플레이어 생존 체크

	public GameObject _uiResult; // Result UI obejct
	public Text _resultText; // Result UI's Text for win or lose
	
	public BoxCollider _attackChkCol; // 플레이어 공격 범위. 켜져있으면 타격 판정이 계속들어가기 때문에 On/Off 필요
	public Rigidbody rigid; // 플레이어 리지드바디
	
	private float _damTimer; // Timer for damage state check
	public GameObject _DamEffect; // Effect for damage state
    public GameObject _DamText; // Text Mesh for damage's value
	
	private float _timerForAttack;
    private float _timerForAttackSnd;
    private bool _attackChkbool;

    Vector2 rotation = Vector2.zero;
    float VelocityChangeLimit = 10.0f;

    // =========================================================================================

    // 플레이어 주변에 적 생성
    private EnemySpawner spawner;

    // 플레이어 카메라
    public Camera playerCamera;

    // ========카메라 관련========================================================================
    // 마우스 움직임에 따른 카메라 스피드
    public float CameraSpeed = 2.0f;
    // 카메라 상하 이동 폭 제한
    public float CameraXLimit = 30.0f;
    [SerializeField] private float m_zoomSpeed = 0f;
    [SerializeField] private float m_zoomMax = 0f;
    [SerializeField] private float m_zoomMin = 0f;
    
    
    // 적 리스폰 타임
    private float spawnTime;
    private float currentTime;
    public Vector2 spawnRandomTime = Vector2.zero;

    // 무적 관련 변수(무적 시간 = 블링크 횟수 * 블링크 스피드)
    bool isInvincible;  // 무적 판정
    [SerializeField] SkinnedMeshRenderer mesh;  // 무적 효과 블링크
    [SerializeField] int blinkCount;    // 블링크 횟수
    [SerializeField] float blinkSpeed;  // 블링크 스피드

    // 플레이어 이펙트
    public GameObject heartEffect;  // 당근 획득 이펙트
    public GameObject magicPrefab;  // 특수 스킬 이펙트
    public GameObject skullEffect;  // 사망 이펙트
    public GameObject picoEffect;   // 망치 이펙트
    private PlanetGravity pg;       // 구체(Planet) 중심으로 중력 적용

    public Transform EffectPos;     // 이펙트가 구현되는 Position
    public Transform PicoPos;

    // Use this for initialization
    void Start () 
	{
        // 스테이지 시작 시 무적 판정
        StartCoroutine(Invincible());

        // 마우스OFF
        MouseCursorOFF();
        
        // 체력 초기화
        _hp = maxHP;
        // 슬라임 스포터
        spawner = GetComponentInChildren<EnemySpawner>();
        // 행성 오브젝트 중력
        pg = GetComponent<PlanetGravity>();
        // 리스폰 타임
        spawnTime = 0;

        //atakState = _rabbit.StringToHash("0_idle"); 
        
        if ( GetComponent<AudioSource>() != null) GetComponent<AudioSource>().Play();
        if(_rabbit != null) 
		{
			_rabbit.speed = 2.0f;
		}
		else
		{
			_rabbit = gameObject.GetComponentInChildren<Animator>();
			_rabbit.speed = 2.0f;
		}


        /*
        if(_uiResult!=null)
        {
            _resultText = _uiResult.transform.Find("3_Result_Text").gameObject.GetComponent<Text>();
        }
		*/

        rigid = GetComponent<Rigidbody>();  // 플레이어 리지드바디
        rigid.freezeRotation = true;        // 리지드 회전값 고정

        if(pg.planet != null)
            rigid.useGravity = false;       // 행성이 있다면 리지드바디의 중력을 끄고 행성 중심 중력으로 대체
        rigid.collisionDetectionMode = CollisionDetectionMode.ContinuousDynamic;    //  콜라이더 충돌감지 모드
        
        rotation.y = transform.eulerAngles.y;   // 카메라 회전값을 플레이어의 회전값으로 맞춤
    }

    private void FixedUpdate()
    {
        // 플레이어가 살아있을 때만
        if (!_playerLive) return;
        // 키 입력이 있을 때만
        if (Input.GetAxis("Vertical") != 0 || Input.GetAxis("Horizontal") != 0)
        {
            Move(); // 이동

            if (_rabbit != null) _rabbit.SetBool("runChk", true);   // 애니메이터가 있을 때만 애니메이션 실행

            // grounded = false;

        }

        /*if ((Input.GetAxis("Vertical") != 0 || Input.GetAxis("Horizontal") != 0))
            {
                float _verticalPos = Input.GetAxis("Vertical") * _speed * Time.deltaTime;
                float _horizonPos = Input.GetAxis("Horizontal") * _speed * Time.deltaTime;

                transform.position += new Vector3(-1 * _horizonPos, 0, -1 * _verticalPos);
                transform.forward = (new Vector3(-1 * _horizonPos, 0, -1 * _verticalPos));
                if(_rabbit != null) _rabbit.SetBool("runChk", true);
            }*/
        else
        {
            // 키 입력이 없는 경우는 runChk false
            if (_rabbit != null) _rabbit.SetBool("runChk", false);
        }
    }

    // Update is called once per frame
    void Update () 
    {
        // 플레이어 생존 시
		if(_playerLive)
		{
            Magic();    // 플레이어 특수 스킬
            
			if((_rabbit != null))
			{
                if (Input.GetButtonDown("Fire1") && !_rabbit.GetCurrentAnimatorStateInfo(0).IsName("1_attack"))
                {
                    // 타격 사운드
                    SoundManager.instance.PlaySoundEffect("Hammer");
                    //picoEffect.SetActive(true);
                    //var pico = Instantiate(picoEffect, EffectPos.position, Quaternion.identity);
                    
                    // 뿅망치 이펙트 코루틴
                    StartCoroutine(picoEffect.GetComponent<PicoHammer>().PicoHammerEF());                   

                    _rabbit.SetBool("attackChk", true);
                    if (_attackChkCol != null)
                    {
                        _attackChkCol.enabled = true;
                    }
                    _attackChkbool = true;
                }

                else
                {
                    if (_attackChkCol != null) _attackChkCol.enabled = false;
                }

                if (_rabbit.GetBool("damageChk"))
                {
                    if (_rabbit.GetCurrentAnimatorStateInfo(0).IsName("3_damage"))
                    {
                        if (_rabbit.GetCurrentAnimatorStateInfo(0).normalizedTime > 0.0f) _rabbit.SetBool("damageChk", false);
                    }
                }              
		    }
        }      
    }

    private void LateUpdate()
    {
        Rotation();    // 플레이어 회전 + 카메라 회전
        CameraZoom();   // 카메라 줌인 줌아웃
    }
    
    // 마우스 커서 ON
    void MouseCursorOFF()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }
    
    // 마우스 커서 OFF
    void MouseCursorON()
    {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }
    
    // 특수기
    void Magic()
    {
        // 스킬 위치를 마우스 클릭 좌표로
        /*Vector3 point = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x,
        Input.mousePosition.y, -Camera.main.transform.position.z));*/

        /*if (Input.GetMouseButton(1))
        {
            Instantiate(previewPrefab, point, Quaternion.LookRotation(point));
        }
        else if (Input.GetMouseButtonUp(1))
        {
            Destroy(previewPrefab);
            Instantiate(magicPrefab, point, Quaternion.identity);
            Destroy(magicPrefab,2f);
        }*/
        
        // 마우스 우클릭 시
        if (Input.GetMouseButtonDown(1))
        {
            var elec = Instantiate(magicPrefab, EffectPos.position, Quaternion.identity);
            SoundManager.instance.PlaySoundEffect("Magic");
            Destroy(elec, 2f);
        }
    }
    
    // 카메라 Zoom
    void CameraZoom()
    {
        float t_zoomDirection = Input.GetAxis("Mouse ScrollWheel");
        
        /*// 카메라 로컬 포지션 y 값이 맥스 값 보다 작고 다이렉션 값이 양수일 때(카메라가 위로 올라감 = 멀어짐)
        if(playerCamera.transform.localPosition.y <= m_zoomMax && t_zoomDirection > 0)
            return;
        // 카메라 로컬 포지션 y 값이 민 값 보다 크고 다이렉션 값이 음수일 때(카메라가 밑으로 내려감 = 가까워짐)
        if(playerCamera.transform.localPosition.y >= m_zoomMin && t_zoomDirection < 0)
            return;*/
        /*Vector3 forward = (transform.position - playerCamera.transform.localPosition).normalized;

        float y = playerCamera.transform.localPosition.y * t_zoomDirection * m_zoomSpeed;

        playerCamera.transform.localPosition = new Vector3(0, y, 0);*/

        if (t_zoomDirection != 0)
        {
            //Mathf.Clamp(playerCamera.transform.localPosition.y, m_zoomMin, m_zoomMax);

            /*if(playerCamera.transform.localPosition.y <= m_zoomMax && t_zoomDirection > 0 || 
               playerCamera.transform.localPosition.y >= m_zoomMin && t_zoomDirection < 0)
                return;*/
            // 카메라 로컬 포지션 y값에 Mathf.Clamp로 카메라의 최소 최대 높이 설정
            playerCamera.transform.localPosition = new Vector3(playerCamera.transform.localPosition.x,
                Mathf.Clamp((playerCamera.transform.localPosition.y + t_zoomDirection), m_zoomMin, m_zoomMax)
                , playerCamera.transform.localPosition.z);
            //playerCamera.transform.localPosition += new Vector3(0, t_zoomDirection, 0);
        }
    }
    
    // 카메라 회전
    void Rotation()
	{
        // Roatation input for the camera & player
        /*rotation.x += -Input.GetAxis("Mouse Y") * CameraSpeed;

        rotation.x = Mathf.Clamp(rotation.x, -CameraXLimit, CameraXLimit);

        playerCamera.transform.localRotation = Quaternion.Euler(rotation.x, 0, 0);*/

        Quaternion localRotation = Quaternion.Euler(0f, Input.GetAxis("Mouse X") * CameraSpeed, 0f);

        transform.rotation = transform.rotation * localRotation;
    }
    
    // 플레이어 이동
	void Move()
	{
        // Calculations for how fast player is moving
        // 캐릭터 정면 방향 계산
        Vector3 forwardDir = Vector3.Cross(transform.up, -playerCamera.transform.right).normalized;
        
        // 캐릭터 측면 방향 계산
        Vector3 rightdirection = Vector3.Cross(transform.up, playerCamera.transform.forward).normalized;
        
        // 캐릭터 속도 계산
        Vector3 targetVelocity = (forwardDir * Input.GetAxis("Vertical") + rightdirection * Input.GetAxis("Horizontal")) * _speed;
        
        //=======================================================================================================
        /*[3] InverseTransformDirection()
            회전 변환만 적용한다.

            월드 공간의 방향 벡터를 트랜스폼의 로컬 공간 방향 벡터로 바꿀 때 사용된다.

            예시 : 내적을 통해 적이 캐릭터의 왼쪽 또는 오른쪽에 있는지 파악하기

        Vector3 enemyPos  = ...;                // 적의 현재 월드 위치
        Vector3 playerPos = transform.position; // 플레이어 캐릭터의 현재 월드 위치

        Vector3 playerToEnemyDir = enemyPos - playerPos;
        Vector3 localEnemyDir = transform.InverseTransformDirection(playerToEnemyDir);

        float d = Vector3.Dot(localEnemyDir, Vector3.right);

        // d >= 0 : 적은 플레이어 캐릭터의 우측에 존재
        // d <  0 : 적은 플레이어 캐릭터의 좌측에 존재*/
        //=======================================================================================================
        
        Vector3 velocity = transform.InverseTransformDirection(rigid.velocity);

        velocity.y = 0;

        velocity = transform.TransformDirection(velocity);

        if (spawner != null)
        {
            // 이동 중 슬라임 리스폰 구현
            currentTime += Time.deltaTime;
            if(currentTime > spawnTime)
            {
                // 리스폰 타임은 인스펙터 창에서 Vector2 값으로 설정
                spawnTime = Random.Range(spawnRandomTime.x, spawnRandomTime.y);
                currentTime = 0;    // 리스폰 타임 초기화
                // spawner.gameObject.transform.Translate(velocity);
                
                // 스폰 포지션을 플레이어 이동 방향 * 3으로 설정
                spawner.gameObject.transform.Translate((forwardDir + rightdirection).normalized * 3f);
                
                // 스포너 메서드 실행
                spawner.SpawnEnemy(spawner.transform);
            }
        }

        Vector3 velocityChange = transform.InverseTransformDirection(targetVelocity - velocity);

        velocityChange.x = Mathf.Clamp(velocityChange.x, -VelocityChangeLimit, VelocityChangeLimit);

        velocityChange.z = Mathf.Clamp(velocityChange.z, -VelocityChangeLimit, VelocityChangeLimit);

        velocityChange.y = 0;

        velocityChange = transform.TransformDirection(velocityChange);

        rigid.AddForce(velocityChange * _speed, ForceMode.Acceleration);
        
    }
	public void Damaged(int _dam)
	{
        // 무적 상태라면 리턴
        if (isInvincible)
            return;
        
        SoundManager.instance.PlaySoundEffect("Damaged");
        // transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y + 0.3f, transform.localPosition.z - 2f); // 피격 시 뒤로 밀리는 판정
        rigid.transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y + 0.3f, transform.localPosition.z - 0.3f); // 피격 시 뒤로 밀리는 판정
        
        // 데미지를 HP에 적용
        _hp -= _dam;
        // Manager.Instance.UpdateHPStatus();
        
        if(!_rabbit.GetCurrentAnimatorStateInfo(0).IsName("3_damage")) _rabbit.SetBool("damageChk", true);
		if(_DamEffect!=null) Instantiate(_DamEffect,new Vector3(transform.position.x, transform.localPosition.y + 2f, transform.position.z),Quaternion.identity);
        if(_DamText!=null) Instantiate(_DamText, new Vector3(transform.position.x, 1.2f, transform.position.z + 0.2f), Quaternion.identity);
        
		/*if(_hp >0)
		{
			if(_hpBar!=null) _hpBar.transform.localScale = new Vector3 (_hp*0.01f,1,1);
			if(_HpVal!=null) _HpVal.text = _hp.ToString();
		}*/
        
        // 체력이 0 이하일 경우
		else if(_hp <= 0)
		{
			// if(_hpBar!=null) _hpBar.transform.localScale = new Vector3 (0,1,1);
			_playerLive=false;
			// if(_HpVal!=null) _HpVal.text = "0";
			_gameWin=false;
            // GameOver();
            // 해골 이펙트
            StartCoroutine(Skull());
		}

        // 무적 코루틴 실행
        StartCoroutine(Invincible());

    }

    // 플레이어 Hp가 0 이하일 때
    IEnumerator Skull()
    {
        yield return new WaitForSeconds(1f);
        var skull = Instantiate(skullEffect, EffectPos.position, transform.rotation);
    }

    // 무적 코루틴
    IEnumerator Invincible()
    {
        // 피격 시 연속 피격 방지용 bool 값
        isInvincible = true;

        // 무적 시간(블링크 횟수 * 블링크 스피드)
        for (int i = 0; i < blinkCount; i++)
        {
            mesh.enabled = !mesh.enabled;   // 스킨 메쉬를 On/Off
            yield return new WaitForSeconds(blinkSpeed);    // blinkSpeed의 속도로 깜빡거린다
        }

        // blinkCount가 끝나면 피격 방지용 bool값 false
        isInvincible = false;
    }

    // 당근 획득 시
    public void HeartEffect()
    {
        var heart = Instantiate(heartEffect, EffectPos.position, EffectPos.rotation);

        Destroy(heart,2f);
    }

	/*public void GameOver()
	{
		//game over
        if (_gameWin)
        {
            if(_resultText != null) _resultText.text = "WIN";
        }
        else
        {
            if(_resultText != null)_resultText.text = "LOSE";
        }
        
        //
		Time.timeScale = 0.0f;
		if(_uiResult != null) _uiResult.SetActive(true);
		
	}*/

    /*public void Regame()
    {
        Time.timeScale = 1.0f;
        Application.LoadLevel("1_play");
    }*/
}
