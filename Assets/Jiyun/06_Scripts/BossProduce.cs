using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class BossProduce : MonoBehaviour
{ 
    #region Variables

    public Transform[] EnemySpawnPoints;             // 에너미 생성 위치 객체들
    private int randPos;                            // 랜덤 생성 위치 번호
    public GameObject[] enemys;                      // 랜덤 생성할 에너미 객체들
    private int randEnemy;                           // 랜덤 생성될 에너미 번호
    public float radius = 3f;                      // 에너미 위치에서 랜덤 생성될 반지름 길이

    #endregion Variables


    #region Unity Method


    // 초기화
    void Start()
    {
        // 5초에 한번씩 아이템 생성 메서드 실행
        InvokeRepeating("SpawnEnemy", 10, 1);
    }

    #endregion Unity Method


    #region Method

    /// <summary>
    /// 아이템 생성 메서드
    /// </summary>
    public void SpawnEnemy(Transform transform)
    {
        // 아이템 생성 위치 개수 만큼 반복
        for (int i = 0; i < EnemySpawnPoints.Length; i++)
        {
            // 랜덤 아이템 지정
            randEnemy = Random.Range(0, enemys.Length);

            // 랜덤 x좌표 지정
            float radomX = Random.Range(-radius, radius);

            // 랜덤 z좌표 지정
            float radomZ = Random.Range(-radius, radius);

            // 랜덤으로 생성될 아이템의 위치 지정
            Vector3 enemyPos = transform.position + new Vector3(radomX, 0, radomZ);

            Vector3 dir = (gameObject.transform.position - transform.position).normalized;

            // 랜덤 아이템 객체 생성
            // Instantiate(생성할 오브젝트, 생성할 위치, 회전 값)
            GameObject Monster = Instantiate(enemys[randEnemy], enemyPos, Quaternion.Euler(dir));
        }
    }

    #endregion Method
}

/*// 시간을 담당할 변수를 하나 만들어준다.
float currTime;
    public GameObject Monster;

    // 반복되는 작업이므로 업데이트 함수 안에서 코드를 입력한다.
    void Update()
    {
        // 시간이 흐르게 만들어준다.
        currTime += Time.deltaTime;

        // 오브젝트를 몇초마다 생성할 것인지 조건문으로 만든다. 여기서는 30초로 했다.
        if (currTime > 10)
        {
            

            // 생성할 오브젝트를 불러온다.
            Instantiate(Monster);

            

            // 시간을 0으로 되돌려주면, 10초마다 반복된다.
            currTime = 0;
        }
    }
}
*/