using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GetItem1 : MonoBehaviour
{

    #region Variables

    private AudioSource audio;
    PlayerScript player;
    public GameObject mainBody;
    private Manager manager;

    #endregion Variables


    #region Unity Method

    private void Start()
    {
        audio = GetComponent<AudioSource>();
        player = FindObjectOfType<PlayerScript>();
        manager = FindObjectOfType<Manager>();
    }



    /// <summary>
    /// OntriggerEnter 콜라이더끼리 충돌시 실행
    /// 아이템 충돌시 실행
    /// </summary>
    /// <param name="other">충돌한 객체</param>
    private void OnTriggerEnter(Collider other)
    {
        //플레이어와 충돌했는지 확인
        if (other.tag == "player")
        {
            Debug.Log("당근");
            // 당근++
            if (gameObject.tag == "Carrot")
            {
                SoundManager.instance.PlaySoundEffect("Carrot");
                manager.UpdateCarrot(1);
                player.HeartEffect();
                Destroy(mainBody);

            }
        }
    }





    #endregion Unity Method



    #region Method



    #endregion Method

}

