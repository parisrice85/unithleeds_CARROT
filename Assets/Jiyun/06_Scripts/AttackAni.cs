using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackAni : MonoBehaviour
{
    public Animator animator; // �ش� ������Ʈ�� Animator ������Ʈ
    private float attackTimer = 0f;
    public float attackInterval = 2f;
    void Start()
    {
        animator = GetComponent<Animator>(); // Animator ������Ʈ�� ������
    }

    // Update is called once per frame
    void Update()
    {
        attackTimer += Time.deltaTime;
        if (attackTimer >= attackInterval)
        {
            attackTimer = 0f;
            animator.SetTrigger("Attack");
        }
    }

}