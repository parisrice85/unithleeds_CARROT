using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class SpawnBoss : MonoBehaviour
{
    public GameObject Boss; // 몬스터 프리팹을 저장할 변수
    //public Vector3 spawnArea; // 몬스터가 생성될 영역
    public GameObject bossSpawnPos;
    private bool isBossSpawned = false; // 보스 몬스터가 생성되었는지 여부를 저장할 변수
    public float userDefinedTime = 60f; // 사용자 입력 시간

    void Start()
    {

        if (!isBossSpawned) // 보스 몬스터가 생성되지 않은 경우에만 생성하도록 합니다.
        {
            StartCoroutine(SpawnBossAfterDelay(userDefinedTime));
        }
    }

    IEnumerator SpawnBossAfterDelay(float delayTime)
    {
        yield return new WaitForSeconds(delayTime); // 사용자 입력 시간이 지나면 일정 시간을 기다립니다.
        //Vector3 spawnPosition = transform.position + new Vector3(Random.Range(-spawnArea.x, spawnArea.x), 0, Random.Range(-spawnArea.z, spawnArea.z));

        //var boss = Instantiate(BossPrefab, bossSpawnPos.transform.position, transform.rotation); // 몬스터를 생성합니다.

        Boss.SetActive(true);

        isBossSpawned = true; // 보스 몬스터가 생성되었음을 저장합니다.
    }


}
