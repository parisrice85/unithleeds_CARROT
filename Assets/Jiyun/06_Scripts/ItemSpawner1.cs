using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemSpawner1 : MonoBehaviour
{
    #region Variables

    public Transform[] itemSpawnPoints;         // 아이템 생성 위치 객체들
    private int randPos;                        // 랜덤 생성 위치 번호
    public GameObject[] items;                  // 랜덤 생성할 아이템 객체들
    private int randItem;                       // 랜덤 생성될 아이템 번호
    public float radius = 10f;                        // 아이템 위치에서 랜덤 생성될 반지름 길이

    #endregion Variables


    #region Unity Method

    // 초기화
    void Start()
    {
        // 몇 초에 한번씩 아이템 생성 메서드 실행
        InvokeRepeating("SpawnItem", 20, 1);
    }

    #endregion Unity Method

    #region Method

    /// <summary>
    /// 아이템 생성 메서드
    /// </summary>
    private void SpawnItem()
    {


        // 아이템 생성 위치 개수 만큼 반복
        for (int i = 0; i < itemSpawnPoints.Length; i++)
        {
            // 랜덤 아이템 지정
            randItem = Random.Range(0, items.Length);

            // 랜덤 x좌표 지정
            float randomX = Random.Range(-radius, radius);

            // 랜덤 z좌표 지정
            float randomZ = Random.Range(-radius, radius);

            // 랜덤으로 생성될 아이템의 위치 지정
            Vector3 itemPos = itemSpawnPoints[i].position + new Vector3(randomX, 0, randomZ);

            // 랜덤 아이템 객체 생성
            // Instantiate(생성할 오브젝트, 생성할 위치, 회전 값)
            GameObject item = Instantiate(items[randItem], itemPos, Quaternion.identity);

            // 몇초 뒤 자동 삭제
            // Destroy(삭제할 오브젝트, 몇초 뒤 삭제할지 시간 지정)
            Destroy(item, 20f);
        }
    }

    #endregion Method
}

