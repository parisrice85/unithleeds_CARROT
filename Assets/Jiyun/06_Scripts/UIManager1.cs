using System.Collections;
using UnityEngine;
using Cinemachine;

public class UIManager1 : MonoBehaviour
{

    #region Variables

    public GameObject settingUI;                        //설정창 오브젝트
    public CinemachineVirtualCamera[] virtualCameras;   // 시네머신에 사용할 가상 카메라
    public float camWait;                               // 카메라 이동간 걸리는 시간
    public AudioSource clickSound;                      // 메뉴 클릭 사운드

    #endregion Variables

    // 인스턴스 화
    public static UIManager1 instance;

    public GameObject TitileMenu;                       // 타이틀 메뉴
    public GameObject button;                           // 버튼

    // 싱글톤(메인메뉴 씬에서만 사용되기에 DonDestryOnLoad에서 제외
    private void Awake()
    {
        if(instance != null )
        {
            DestroyImmediate(this.gameObject);
            return;
        }
        instance = this;
        // DontDestroyOnLoad(gameObject);
    }

    private void Start()
    {
        // 마우스 커서 On
        MouseCursorON();
        // 타이틀 메뉴 On
        TitileMenu.SetActive(true);
        // 버튼 On
        button.SetActive(true);

        // 가상 카메라 값 초기화
        for (int i = 0; i < virtualCameras.Length; i++)
        {
            virtualCameras[i].Priority = 1;
        }

        // 가장 먼저 보여줄 카메라 설정
        virtualCameras[0].Priority = 10;
    }
    void MouseCursorOFF()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }
    void MouseCursorON()
    {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }

    #region Method
    /// <summary>
    /// 시작 버튼 누를 시 실행
    /// </summary>
    public void BtnStartClick() 
    {
        clickSound.Play();
        StartCoroutine(Cinemachine());  // 가상 카메라 연출
    }

    // 가상 카메라 연출 코루틴
    IEnumerator Cinemachine()
    {
        // 버튼 메뉴 비활성화
        button.SetActive(false);

        // 2번 카메라로 이동
        virtualCameras[1].Priority = 11;
        yield return new WaitForSeconds(camWait);

        // 3번 카메라로 이동
        virtualCameras[2].Priority = 12;
        yield return new WaitForSeconds(camWait);

        // TitileMenu.SetActive(false);

        // 씬 이동 연출 에셋 ==> Initiate.Fade(씬 이름, 페이드 컬러, 페이드 시간)
        Initiate.Fade("02_Prologue", Color.black, 2f);
        // SceneManager.LoadScene("1_Play");
    }


    public void BtnSettingClick() 
    {
        clickSound.Play();

        //설정창이 비활성화 되어있을 경우 활성화 한다.
        if (settingUI.activeSelf== false) 
        {
            //설정 UI 오브젝트 활성화
            settingUI.SetActive(true);
      
        }
        //설정창이 활성화 되어있을 경우 비활성화 한다
        else
        {  
            //설정 UI 오브젝트 비활성화
            settingUI.SetActive(false);
        }       
    }


    /// <summary>
    /// 나가기 버튼 누를 시 실행 = 프로그램 종료
    /// </summary>
    public void BtnExitClick()
    {
#if UNITY_EDITOR
        // 실행 중인 어플리케이션을 종료
        UnityEditor.EditorApplication.isPlaying = false;
#else
        // 어플리케이션 나가기
        Application.Quit();
#endif
    }

    /*clickSound.Play();

#if UNITY_EDITOR
        //실행 중인 어플리케이션을 종료
        UnityEditor.EditorApplication.isPlaying = false;
#else
        //어플리케이션 나가기
        Application.Quit();
#endif*/


    #endregion Method
}
