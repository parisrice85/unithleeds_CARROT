using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlameCon : MonoBehaviour
{
    // 스테이지 3 불꽃 연출
    public GameObject[] flame;

    // 불꽃 이펙트 초기화
    void Start()
    {
        for (int i = 0; i < flame.Length; i++)
        {
            flame[i].SetActive(false);
        }
    }

    // 일정 구역에 플레이어 진입 시 불꽃 이펙트 On
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("player"))
        {
            for (int i = 0; i < flame.Length; i++)
            {
                flame[i].SetActive(true);
            }
        }
    }
}
