using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Sound  // 효과음 담당 클래스 생성
{
    public string soundName;    // 변수로 쓸 효과음 이름
    public AudioClip clip;      // 오디오 클립(MP3)
}

public class SoundManager : MonoBehaviour
{
    // 사운드매니저에 대한 접근을 용이하게 인스턴스 화
    public static SoundManager instance;

    [SerializeField] Sound[] sfxSounds; // 효과음 배열

    [Header("효과음 플레이어")]   
    [SerializeField] AudioSource[] sfxPlayer;   // 사용할 오디오소스(오디오 플레이어)

    private void Start()
    {
        // 싱글톤
        if (instance != null)
        {
            // 씬에 두 개 이상 있지 않도록
            DestroyImmediate(this.gameObject);
            return;
        }
        instance = this;
        DontDestroyOnLoad(gameObject);
    }

    // 외부에서 호출될 메서드(효과음 이름을 받으면 출력)
    public void PlaySoundEffect(string _soundName)
    {
        // 설정한 효과음 수만큼 for문
        for (int i = 0; i < sfxSounds.Length; i++)
        {
            // 매개변수와 설정된 효과음 이름이 같다면
            if(_soundName == sfxSounds[i].soundName)
            {
                // 오디오 플레이어 배열 검색
                for (int x = 0; x < sfxPlayer.Length; x++)
                {
                    // 빈 오디오 플레이어가 있다면
                    if (!sfxPlayer[x].isPlaying)
                    {
                        // 빈 오디오 플레이어에 효과음 파일 삽입
                        sfxPlayer[x].clip = sfxSounds[i].clip;
                        sfxPlayer[x].Play();    // 플레이
                        return;     // 효과음을 찾았기 때문에 반복문 빠져나오기
                    }
                }
                
                Debug.Log("모든 효과음 플레이어가 사용 중");
                return;
            }
        }
        Debug.Log("해당 이름의 사운드가 없습니다");
    }
}
