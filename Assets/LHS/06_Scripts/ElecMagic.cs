using System.Collections;
using UnityEngine;

public class ElecMagic : MonoBehaviour
{
    public int damage;
    public float attackRate;
    EnemyHealth enemy;



    private void OnTriggerStay(Collider other)
    {
        if (other.transform.CompareTag("enemy"))
        {
            enemy = other.GetComponent<EnemyHealth>();
            enemy.ApplayDamage(damage);
            //StartCoroutine("AttackRate");
        }
    }

    IEnumerable AttackRate()
    {
        yield return new WaitForSeconds(attackRate);
        enemy.ApplayDamage(damage);
    }

}
