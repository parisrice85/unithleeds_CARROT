using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopupFalse : MonoBehaviour
{
    // Update is called once per frame
    void Update()
    {
        StartCoroutine(("WaitPopUp"));

    }
    
    // 팝업창을 최소 1초 유지
    IEnumerator WaitPopUp()
    {
        yield return new WaitForSeconds(1f);
        if(Input.anyKey)
            gameObject.SetActive(false);
    }
}
