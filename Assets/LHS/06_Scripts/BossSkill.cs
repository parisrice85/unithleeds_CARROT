using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossSkill : MonoBehaviour
{
    public int damage;              // 스킬 데미지
    public float attackRate;        // 쿨타임
    public PlayerScript player;     // 플레이어 스크립트



    private void OnTriggerStay(Collider other)
    {
        // 스킬 범위 안에 플레이어가 있다면
        if (other.transform.CompareTag("player"))
        {
            // 데미지
            player = other.GetComponent<PlayerScript>();
            player.Damaged(damage);
        }
    }
}
