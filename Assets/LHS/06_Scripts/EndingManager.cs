using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EndingManager : MonoBehaviour
{
    // PRESS ANY KEY 안내 UI
    public Text pressAnyKey;
    // 업데이트에서 감지할 Bool값
    public bool isEnd;

    // Start is called before the first frame update
    void Start()
    {
        isEnd = false;
        StartCoroutine(ReStart());
    }

    private void Update()
    {
        // 키 입력을 받으면 메인메뉴 씬으로 이동
        if (isEnd && Input.anyKey)
        {
            pressAnyKey.gameObject.SetActive(false);
            Initiate.Fade("01_MainMenu_Scene", Color.black, 2f);
        }
    }

    // 엔딩롤 대기용 코루틴
    IEnumerator ReStart()
    {
        yield return new WaitForSeconds(51f);

        // 키 입력 UI
        pressAnyKey.gameObject.SetActive(true);

        // UI와 동시에 Bool값 리턴
        isEnd = true;

    }
}
