using UnityEngine;

public class ControlUIPopup : MonoBehaviour
{   
    // 컨트롤 설명 UI
    public GameObject ControlUIPanel;
    private bool isConUIOn;
    
    // 맵의 일정 장소에 플레리어가 위치하면 UI 호출
    private void OnTriggerEnter(Collider other)
    {
        if (!other.CompareTag("player")) return;
        ControlUIPanel.SetActive(true);
    }
}
