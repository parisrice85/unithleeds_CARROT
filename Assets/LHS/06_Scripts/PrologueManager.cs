using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PrologueManager : MonoBehaviour
{
    public Text[] texts;                                // 프롤로그 텍스트
    public CinemachineVirtualCamera[] virtualCameras;   // 버추얼 카메라 배열
    public GameObject prologuePanel;                    // 프롤로그 패널
    public Text skipText;                               // 프롤로그 스킵 텍스트
    
    // Start is called before the first frame update
    void Start()
    {
        // 버추얼 카메라 초기화
        for (int i = 0; i < virtualCameras.Length; i++)
        {
            virtualCameras[i].Priority = 1;
        }
        
        // 첫 카메라 설정
        virtualCameras[0].Priority = 2;
        
        // 카메라 코루틴 실행
        StartCoroutine(CameraMove());
    }

    // Update is called once per frame
    void Update()
    {
        // 키 입력이 있다면 프롤로그 종료
        if (Input.anyKey)
        {
            prologuePanel.SetActive(false);
            Initiate.Fade("03_Castle", Color.black, 2f);
        }
    }

    // 카메라를 이용한 프롤로그 연출
    IEnumerator CameraMove()
    {
        texts[0].gameObject.SetActive(true);
        yield return new WaitForSeconds(5f);
        
        texts[0].gameObject.SetActive(false);
        texts[1].gameObject.SetActive(true);
        virtualCameras[1].Priority = 3;
        yield return new WaitForSeconds(3f);

        texts[1].gameObject.SetActive(false);
        texts[2].gameObject.SetActive(true);
        virtualCameras[2].Priority = 4;
        yield return new WaitForSeconds(3f);
        
        texts[2].gameObject.SetActive(false);
        texts[3].gameObject.SetActive(true);
        virtualCameras[3].Priority = 5;
        yield return new WaitForSeconds(3f);
        
        texts[3].gameObject.SetActive(false);
        texts[4].gameObject.SetActive(true);
        
        // 프롤로그 마지막이 되면 스킵 문구 변경
        skipText.text = "PRESS ANY KEY";
    }
}
