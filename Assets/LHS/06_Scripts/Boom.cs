using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boom : MonoBehaviour
{
    // 폭발 이펙트 오브젝트
    public GameObject[] particleBoom;

    /*private void OnCollisionEnter(Collision other)
    {
        ContactPoint contactPoints = other.contacts[0];

        if (other.transform.name == "Bullet")
        {
            var clone = Instantiate(particleBoom, contactPoints.point, Quaternion.identity);
            Destroy(clone, 1f);
        }

    }*/

    public void BoomEffect(Transform position)
    {
        // var clone = Instantiate(particleBoom[0], position.position, Quaternion.LookRotation(-(position.forward) / 2));
        // 폭발 이펙트 생성
        var clone = Instantiate(particleBoom[0], position.position, Quaternion.identity);

        Destroy(clone, 2f);
    }

    public void BoomEffect2(Transform position)
    {
        // 폭발 이펙트2 생성
        var clone = Instantiate(particleBoom[1], position.position, Quaternion.identity);
        Destroy(clone, 2f);
    }

}
