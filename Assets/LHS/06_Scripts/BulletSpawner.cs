using DG.Tweening;
using UnityEngine;

public class BulletSpawner : MonoBehaviour
{
    public GameObject bulletPrefab;
    public float spawnRateMin = 2f;
    public float spawnRateMax = 5f;

    private Transform target;
    private float spawnRate;
    private float timeAfterSawn;

    private Vector3 firstPos;
    private Vector3 secondPos;
    private Vector3 thirdPos;

    Vector3 up;
    Vector3 left;
    Vector3 right;

    // Start is called before the first frame update
    void Start()
    {
        timeAfterSawn = 0f;

        spawnRate = Random.Range(spawnRateMin, spawnRateMax);

        target = FindObjectOfType<PlayerScript>().transform;

        /*firstPos = transform.position;
        secondPos = new Vector3(0, (firstPos.y + thirdPos.y) / 2, (firstPos.z + thirdPos.z) / 2);
        thirdPos = firstPos + target.position;*/

        up = new Vector3(0, transform.localPosition.y + 1f, 0);
        left = new Vector3(transform.localPosition.x - 1f, 0, 0);
        right = new Vector3(transform.localPosition.x + 1f, 0, 0);


        firstPos = transform.localPosition;
        thirdPos = firstPos + target.position;
        secondPos = (firstPos + thirdPos) / 2;
    }

    // Update is called once per frame
    void Update()
    {
        if ((target.position - transform.position).magnitude <= 1f)
        {
            return;
        }

        timeAfterSawn += Time.deltaTime;

        if (timeAfterSawn >= spawnRate)
        {
            timeAfterSawn = 0f;



            GameObject bullet
                = Instantiate(bulletPrefab, transform.position, transform.rotation);


            bullet.transform.LookAt(target);

      
            spawnRate = Random.Range(spawnRateMin, spawnRateMax);

           // transform.DOPath(new[] { secondPos, firstPos + Vector3.up, secondPos + Vector3.left * 2, thirdPos, secondPos + Vector3.right * 2, thirdPos + Vector3.up }, 1f, PathType.CubicBezier).SetEase(Ease.Unset);
            transform.DOPath(new[] { secondPos, firstPos + up, secondPos + left * 2, thirdPos, secondPos + right * 2, thirdPos + up }, 1f, PathType.CubicBezier).SetEase(Ease.Unset);

        }
    }


}
