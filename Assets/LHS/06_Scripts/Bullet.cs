using DG.Tweening;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public float speed = 8f;
    private Rigidbody bulletRigidbody;
    private Transform player;
    private Camera cam;
    public ParticleSystem explosion;


    private Vector3 camOriginPos;

    private Boom planetBoom;

    // Start is called before the first frame update
    void Start()
    {
        //InvokeRepeating("CamOriginPos", 1, 2);

        planetBoom = FindAnyObjectByType<Boom>();
        cam = Camera.main;
        camOriginPos = cam.transform.localPosition;

        player = FindAnyObjectByType<PlayerScript>().transform;
        bulletRigidbody = GetComponent<Rigidbody>();
        bulletRigidbody.velocity = transform.forward * speed;
        /*Vector3 firstPos = transform.position;
        Vector3 thirdPos = firstPos + player.position;
        Vector3 secondPos = (firstPos + thirdPos) / 2;
        transform.DOPath(new[] { secondPos, firstPos + Vector3.up, secondPos + Vector3.left * 2, thirdPos, secondPos + Vector3.right * 2, thirdPos + Vector3.up }, 1f, PathType.CubicBezier).SetEase(Ease.Unset);*/

        Vector3 up = new Vector3(0, transform.localPosition.y + 1f, 0);
        Vector3 left = new Vector3(transform.localPosition.x - 1f, 0, 0);
        Vector3 right = new Vector3(transform.localPosition.x + 1f, 0, 0);


        Vector3 firstPos = transform.localPosition;
        Vector3 thirdPos = firstPos + player.position;
        Vector3 secondPos = (firstPos + thirdPos) / 2;
        transform.DOPath(new[] { secondPos, firstPos + up, secondPos + left * 2, thirdPos, secondPos + right * 2, thirdPos + up }, 1f, PathType.CubicBezier).SetEase(Ease.Unset);

    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "player")
        {
            PlayerScript playerScript = other.GetComponent<PlayerScript>();

            if (playerScript != null )
            {
                playerScript.Damaged(1);
                /*Rigidbody rb = other.gameObject.GetComponent<Rigidbody>();
                rb.AddExplosionForce(10f, transform.position, 5f);*/

                explosion.Play();
                planetBoom.BoomEffect2(transform);
                cam.transform.DOShakePosition(0.005f);
                Destroy(gameObject);
            }
        }
        else if (other.tag == "Planet")
        {
            Debug.Log("땅충돌");
            explosion.Play();
            planetBoom.BoomEffect(transform);
            cam.transform.DOShakePosition(0.005f);
            Destroy(gameObject);
        }
        SoundManager.instance.PlaySoundEffect("Cannon");
        CamOriginPos();
        explosion.Play();
        Debug.Log("그 외 충돌");

    }

    void CamOriginPos()
    {
        transform.localPosition = camOriginPos;
    }

    /*void Explosion()
    {
        var exp = Instantiate(explosion, player.transform.position, Quaternion.identity);
        Destroy(exp, 1f);
    }*/
}
