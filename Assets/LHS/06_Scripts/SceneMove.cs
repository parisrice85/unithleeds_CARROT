using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneMove : MonoBehaviour
{

    public GameObject stageMenu;
    public GameObject RocketSmoke;
    private AudioSource rocketSound;

    private void Start()
    {
        rocketSound = GetComponent<AudioSource>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.CompareTag("player"))
        {
            /*stageMenu.SetActive(true);
            Time.timeScale = 0;*/

            StartCoroutine(GameStart());
        }
    }

    IEnumerator GameStart()
    {
        Instantiate(RocketSmoke, transform.position, Quaternion.identity);
        Camera.main.DOShakePosition(1f);
        rocketSound.Play();
        yield return new WaitForSeconds(1f);

        Initiate.Fade("04_MJY_Scene", Color.black, 2f);
    }


    /*public void MJYStageBtn()
    {
        Time.timeScale = 1;
        stageMenu.SetActive(false);
        Initiate.Fade("03_MJY_Scene", Color.black, 2f);
    }

    public void PJYStageBtn()
    {
        Time.timeScale = 1;
        stageMenu.SetActive(false);
        Initiate.Fade("���̸�", Color.black, 2f);
    }

    public void LHSStageBtn()
    {
        Time.timeScale = 1;
        stageMenu.SetActive(false);
        Initiate.Fade("04_LHS_Scene", Color.black, 2f);
    }*/
}
