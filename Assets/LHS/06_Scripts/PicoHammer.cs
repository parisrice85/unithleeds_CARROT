using System.Collections;
using UnityEngine;

public class PicoHammer : MonoBehaviour
{
    public GameObject[] picoEffect;     // 뿅망치 이펙트 배열
    public float picoRange;             // 뿅망치 범위

    // Start is called before the first frame update
    void Start()
    {

    }
    
    // 뿅망치 코루틴
    public IEnumerator PicoHammerEF()
    {   
        // 뿅망치 이펙트 랜덤 입력 값
        int ran = Random.Range(0, picoEffect.Length);
        
        yield return new WaitForSeconds(0.1f);  // 대기 시간
        
        // 뽕망치 랜덤 생성
        var picoHammer = Instantiate(picoEffect[ran], transform.position, transform.rotation);
        
        // 현재 위치에서 picoRange 반지름, Enemy 레이어를 배열에 저장
        RaycastHit[] raycastHits =
            Physics.SphereCastAll(transform.position, picoRange, Vector3.up, 
                0f, LayerMask.GetMask("Enemy"));
        // foreach 문으로 raycastHits 배열 호출
        foreach (RaycastHit hitObj in raycastHits)
        {
            // Enemy 스크립트 속 HitbyPico() 메서드 호출
            hitObj.transform.GetComponent<Enemy>().HitbyPico(transform.position);
        }
        
        
        /*if (t_cols.Length > 0)
        {
            for (int i = 0; i < t_cols.Length; i++)
            {
                if (t_cols[i].CompareTag("enemy"))
                {
                    t_cols[i].GetComponent<Rigidbody>().AddExplosionForce(50f, transform.position, 10f);
                    Debug.Log("슬라임바디");
                }
            }
        }*/
        // 뿅망치 애니메이션 속도와 소리를 맞추기 위해 대기시간
        yield return new WaitForSeconds(0.2f);
        Debug.Log("피코피코");
        Destroy(picoHammer);    // 이펙트 제거
    }

    // Update is called once per frame
/*    void Update()
    {
        if(pico == null)
            gameObject.SetActive(false);
    }*/

}
