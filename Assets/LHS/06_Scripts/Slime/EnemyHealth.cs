using UnityEngine;
using UnityEngine.UI;

public class EnemyHealth : LivingEntity
{
    #region Variables

    private Animator animator;          // 에너미 애니메이터 컴포넌트
    Manager manager;                    // 스테이지 클리어 호출
    public Slider hpSlider;             // 보스 체력 슬라이더
    public GameObject hpBarPanel;       // 보스 체력 패널
    public bool autoDie;                // 일반 슬라임 개체 수 조절

    #endregion Variables


    #region Unity Method


    // 초기화
    void Start()
    {
        // 일반 슬라임일 경우 자동 파괴
        if (autoDie)
        {
            Destroy(gameObject, 20f);
        }
        manager = FindAnyObjectByType<Manager>();
        // 초기화 실행 메서드
        Init();
    }

    // 매 프레임마다 반복
    void Update()
    {
        // 보스일 경우 보스 클리어 패널
        if (gameObject.layer == 8)
        {
            // 체력 패널 실시간 반영
            hpBarPanel.SetActive(true);
            BossHpBar();

            if (isDie)
            {
                // 보스 클리어 시
                hpBarPanel.SetActive(false);
                manager.BossClear();
            }
        }
    }

    void BossHpBar()
    {
        hpSlider.maxValue = startHealth;
        hpSlider.value = currentHealth;
    }

    #endregion Unity Method


    /// <summary>
    /// 초기화 실행 메서드
    /// </summary>
    #region Method

    protected override void Init()
    {
        // 체력과 죽음 여부 초기화
        base.Init();

        animator = GetComponent<Animator>();
    }

    /// <summary>
    /// 체력을 회복하는 메서드
    /// </summary>
    /// <param name="newHealth">회복할 체력 수치</param>
    public override void RestoreHealth(int newHealth)
    {
        base.RestoreHealth(newHealth);
    }

    /// <summary>
    /// 에너미 데미지 처리
    /// </summary>
    /// <param name="damage">받은 데미지</param>
    /// <returns></returns>
    public override bool ApplayDamage(int damage)
    {
        // 생명체가 무적상태이거나 생명체가 죽었을 경우는
        // 데미지 처리 불가
        if (base.ApplayDamage(damage) == false)
        {
            return false;
        }

        // 에너미 데미지 애니메이션 실행
        animator.SetTrigger("IsDamage");
        Debug.Log(damage + " 의 데미지!!");
        /*// 플레이어 공격력만큼 에너미 체력 감소
        currentHealth -= damage;

        // 체력이 0이하이면 죽음 처리
        Die();*/



        // 데미지 처리 완료
        return true;
    }

    /// <summary>
    /// 사망 처리
    /// </summary>
    public override void Die()
    {
        base.Die();

        // 죽지 않았으면 메서드 종료
        if (!isDie)
            return;

        // 에너미 죽음 애니메이션 실행
        animator.SetTrigger("IsDie");
        SoundManager.instance.PlaySoundEffect("Slime");

        Debug.Log("끄앙 슬라임 ㅠ");
        // 죽음 애니메이션 실행 후 에너미 객체 삭제
        Destroy(gameObject, 1f);
    }

    public bool DieCheck()
    {
        return isDie;
    }

    #endregion Method
}
