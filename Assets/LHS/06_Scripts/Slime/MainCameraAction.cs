using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// TPS 용 카메라!
public class MainCameraAction : MonoBehaviour
{
    #region Variables

    public GameObject target;           // 카메라가 쫓아다닐 타겟 객체(플레이어)
    public Transform cameraArm;         // 카메라의 좌표값

    public float xSpeed = 0.0f;        // 카메라 x축 스피드         
    public float ySpeed = 0.0f;        // 카메라 y축 스피드
    public float yMinLimit = 0.0f;      // 카메라 y축(상하) 최소 높이 제한
    public float yMaxLimit = 0.0f;      // 카메라 y축(상하) 최대 높이 제한
    public float offsetX = 0.0f;        // 카메라의 x좌표
    public float offsetY = 3.0f;        // 카메라의 y좌표
    public float offsetZ = -3.0f;        // 카메라의 z좌표
    public float maxOffset = -3.5f;     // 타겟으로부터의 카메라의 최대 위치
    public float minOffset = -0.5f;     // 타겟으로부터의 카메라의 최소 위치

    // 카메라 회전축 및 좌표를 위한 변수
    private float x = 0.0f;
    private float y = 0.0f;

    #endregion Variables


    #region Unity Method

    /// <summary>
    /// Update 메서드 이후 진행되는 메서드
    /// </summary>
    private void LateUpdate()
    {
        // 카메라 좌표 설정
        LookCamera();
    }


    #endregion Unity Method


    #region Method

    /// <summary>
    /// 카메라 좌표 설정
    /// </summary>
    private void LookCamera()
    {
        // 카메라 x축 설정
        x += Input.GetAxis("Mouse X") * xSpeed * Time.deltaTime;

        // 카메라 y축 설정
        y -= Input.GetAxis("Mouse Y") * ySpeed * Time.deltaTime;

        // 카메라 오프셋 z축 설정
        if(offsetZ <= maxOffset)
        {
            offsetZ = maxOffset;
        }
        else if(offsetZ >= minOffset)
        {
            offsetZ = minOffset;
        }

        // 카메라의 앵글 값의 최소, 최대 제한값 구하기
        y = ClamAngle(y, yMinLimit, yMaxLimit);

        // 카메라 회전 값
        Quaternion rotation = Quaternion.Euler(y, x, 0);

        // 카메라 위치
        Vector3 position = rotation * new Vector3(0, 0, offsetZ) +
            target.transform.position + new Vector3(0, offsetY, 0);

        // 카메라 트랜스폼 회전값 설정
        transform.rotation = rotation;

        // 카메라 트랜스폼 위치값 설정
        transform.position = position;

    }

    /// <summary>
    /// 카메라의 앵글 값의 최소, 최대 제한
    /// </summary>
    /// <param name="angle">카메라 각도</param>
    /// <param name="min">최소 각도</param>
    /// <param name="max">최대 각도</param>
    /// <returns></returns>
    private float ClamAngle(float angle, float min, float max)
    {
        if(angle < -360)
        {
            angle += 360;
        }

        else if(angle > 360)
        {
            angle -= 360;
        }

        return Mathf.Clamp(angle, min, max);
    }


    #endregion Method
}
