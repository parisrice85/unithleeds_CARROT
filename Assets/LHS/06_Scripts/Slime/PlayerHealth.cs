using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 플레이어 건강상태 괸리생명체 뼈대 클래스 상속 받기
/// </summary>
public class PlayerHealth : LivingEntity
{
    #region Variables

    private Animator animator;          // 플레이어 애니메이터 컴포넌트
    public int defence;

    private CapsuleCollider collider;
    #endregion Variables


    #region Unity Method


    // 초기화
    void Start()
    {
        // 초기화 실행 메서드
        Init();
    }

    // 매 프레임마다 반복
    void Update()
    {

    }


    #endregion Unity Method



    #region Method

    /// <summary>
    /// 초기화 실행 메서드
    /// </summary>
    protected override void Init()
    {
        // 체력과 죽음 여부 초기화
        base.Init();

        animator = GetComponent<Animator>();
        GameSceneUIManager.Instance.ChangeDefenceValue(defence);    // 방어력 UI 초기화
    }

    /// <summary>
    /// 체력을 회복하는 메서드
    /// </summary>
    /// <param name="newHealth">회복할 체력 수치</param>
    public override void RestoreHealth(int newHealth)
    {
        base.RestoreHealth(newHealth);
    }

    /// <summary>
    /// 플레이어 데미지 처리
    /// </summary>
    /// <param name="damage">받은 데미지</param>
    /// <returns></returns>
    public override bool ApplayDamage(int damage)
    {
        damage -= defence;
        if(damage < 0)
        {
            damage = 1;
        }

        // 생명체가 무적상태이거나 생명체가 죽었을 경우는
        // 데미지 처리 불가
        if (base.ApplayDamage(damage) == false)
        {
            return false;
        }

        // 플레이어 데미지 애니메이션 실행
        animator.SetTrigger("IsDamage");

        // 에너미 공격력 만큼 플레이어 체력 감소
        // currentHealth -= damage;
        Debug.Log(damage + " 의 데미지를 입었습니다.");

        // 플레이어 체력바 수치 변경
        GameSceneUIManager.Instance.ChangeHPValue();

        // 체력이 0 이하이면 죽음 처리
        // Die();

        // 데미지 처리 완료
        return true;
    }

    /// <summary>
    /// 사망 처리
    /// </summary>
    public override void Die()
    {
        base.Die();

        // 죽지 않았으면 메서드 종료
        if (!isDie)
            return;

        // 플레이어 죽음 애니메이션 실행
        animator.SetTrigger("IsDie");
        gameObject.tag = "Finish";
        gameObject.layer = 0;

        Debug.Log("끄앙 주금");
    }

    public void IncreaseDef(int def)
    {
        defence += def;
    }

    #endregion Method
}
