 using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpinItem : MonoBehaviour
{
    // 아이템 띄우기
    void Start()
    {
        transform.position = transform.position + Vector3.up;
    }

    // 아이템 y축회전
    void Update()
    {
        transform.Rotate(new Vector3(0,90f,0) * Time.deltaTime);
    }
}
