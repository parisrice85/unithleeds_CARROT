using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour
{
    #region Variables

    public GameObject settingUI;        // 설정창 오브젝트



    #endregion Variables


    #region Method

    /// <summary>
    /// 시작 버튼 누를 시 실행
    /// </summary>
    public void BtnStartClick()
    {
        SceneManager.LoadScene("02_GameScene");
    }

    /// <summary>
    /// 설정 버튼 누를 시 실행
    /// </summary>
    public void BtnSettingClick()
    {
        // 설정창이 비활성화 되어있을 경우 활성화 한다.
        if (settingUI.activeSelf == false)
        {
            // 설정UI 오브젝트 활성화
            settingUI.SetActive(true);
        }

        // 설정창이 활성화 되어있을 경우 비활성화 한다.
        else
        {
            // 설정UI 오브젝트 비활성화
            settingUI.SetActive(false);
        }
    }

    /// <summary>
    /// 나가기 버튼 누를 시 실행 = 프로그램 종료
    /// </summary>
    public void BtnExitClick()
    {
#if UNITY_EDITOR
        // 실행 중인 어플리케이션을 종료
        UnityEditor.EditorApplication.isPlaying = false;
#else
        // 어플리케이션 나가기
        Application.Quit();
#endif
    }


#endregion Method
}
