using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameSceneUIManager : MonoBehaviour
{
    #region Variables

    // 싱글톤
    // 다른 클래스에서 자유롭게 접근가능
    // 단 하나의 스크립트만 존재할 수 있음
    public static GameSceneUIManager instance;

    public static GameSceneUIManager Instance
    {
        get
        {
            if(instance == null)
            {
                // 만약에 GameSceneUIManager생성되지 않았을 경우
                // 씬에서 GameSceneUIManager.를 찾아서 할당
                instance = FindObjectOfType<GameSceneUIManager>();
            }
            return instance;
        }
    }

    public Slider hpBar;                // 플레이어 체력바
    public Text hpTexts;                // 플레이어 체력 수치를 나타내는 텍스트
    public PlayerHealth playerHealth;   // 플레이어 건강 관리 클래스

    public Text attackText;             // 플레이어 공격력 수치를 나타내는 텍스트
    public Text DefenceText;              // 플레이어 소지금을 나타내는 텍스트


    #endregion Variables


    #region Unity Method


    // 초기화
    void Start()
    {
        instance = this;
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    // 매 프레임마다 반복
    void Update()
    {
        // 플레이어 체력바 수치 변경
        ChangeHPValue();
    }


    #endregion Unity Method


    #region Method

    /// <summary>
    /// 플레이어 체력바 수치 변경
    /// </summary>
    public void ChangeHPValue()
    {
        // UI 슬라이더 최대 value 1이기 때문에
        // 체력 수치에서 100을 나눠서 설정한다 ( 현재체력 / 최대체력)
        hpBar.value = (float)playerHealth.currentHealth / 100;

        // 플레이어 체력 수치를 나타내는 텍스트 데이터를
        // 현재 플레이어 체력 수치 값으로 설정
        hpTexts.text = playerHealth.currentHealth.ToString();
    }
    
    /// <summary>
    /// 플레이어 공격력 수치 텍스트를 바꾸는 메서드
    /// </summary>
    /// <param name="attackData">변경될 공격력</param>
    public void ChangeAttackValue(int attackData)
    {
        attackText.text = attackData.ToString();
    }

    /// <summary>
    /// 플레이어 소지금 텍스트를 바꾸는 메서드
    /// </summary>
    /// <param name="moneyData">변경될 소지금</param>
    public void ChangeDefenceValue(int defenceData)
    {
        DefenceText.text = defenceData.ToString();
    }


    /// <summary>
    /// 씬 초기화 버튼 = 새로고침 버튼
    /// </summary>
    public void BtnRefresh()
    {
        SceneManager.LoadScene("02_GameScene");
    }

    /// <summary>
    /// 게임 종료 버튼
    /// </summary>
    public void BtnExit()
    {
#if UNITY_EDITOR
        // 실행중인 프로그램 정지
        UnityEditor.EditorApplication.isPlaying = false;
#else
        // 프로그램 종료
        Application.Quit();
#endif
    }
#endregion Method
}
