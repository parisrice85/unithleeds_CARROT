using Cinemachine;
using System.Collections;
using UnityEngine;

public class CinemachineMove : MonoBehaviour
{
    public GameObject virtualCameraParent;              // 버추얼 카메라 부모 오브젝트
    public CinemachineVirtualCamera[] virtualCameras;   // 버추얼 카메라 배열
    GameObject playerCam;                               // 플레이어 자식 카메라(메인 카메라)

    // Start is called before the first frame update
    void Start()
    {
        playerCam = GameObject.Find("PlayerCam");       // 플레이어 자식 카메라
        
        playerCam.SetActive(false);                     // 플레이어 자식 카메라 OFF
        virtualCameraParent.SetActive(true);            // 버추얼 카메라 ON
        
        // 버추얼 카메라 초기화
        for (int i = 0; i < virtualCameras.Length; i++)
        {
            virtualCameras[i].enabled = true;
            virtualCameras[i].Priority = 1;
        }
        
        // 1번 버추얼 카메라 셋팅
        virtualCameras[0].Priority = 5;
        // 코루틴 실행
        StartCoroutine(CameraMove());
    }
    IEnumerator CameraMove()
    {
        yield return new WaitForSeconds(1.5f);
        virtualCameras[1].Priority = 6;
        yield return new WaitForSeconds(2f);
        virtualCameras[2].Priority = 7;
        yield return new WaitForSeconds(2f);
        
        // 연출이 끝난 버추얼 카메라 OFF
        foreach (var t in virtualCameras)
        {
            t.enabled = false;
        }
        
        // 버추얼 카메라 부모 오브젝트 OFF
        virtualCameraParent.SetActive(false);   // 버추얼 카메라 부모 오브젝트 OFF
        gameObject.SetActive(false);            // 메인 카메라(플레이어 밖) OFF
    }

    private void OnDisable()
    {
        playerCam.SetActive(true);              // 메인 카메라(플레이어 자식) ON
    }
}
