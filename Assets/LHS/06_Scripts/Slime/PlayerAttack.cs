using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SocialPlatforms.Impl;

public class PlayerAttack : MonoBehaviour
{
    #region Variables

    public Collider other;              // 충돌한 객체
    public Animator animator;           // 플레이어 애니메이터 컴포넌트
    public int attack;                  // 플레이어 공격력

    #endregion Variables


    #region Unity Method

    private void Start()
    {
        GameSceneUIManager.Instance.ChangeAttackValue(attack);     
    }

    /// <summary>
    /// 콜라이더 충돌 시 메서드 실행
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerStay(Collider other)
    {
        // 충돌된 객체 저장
        this.other = other;

        // 공격 실행
        Attack();
    }

    /// <summary>
    /// 콜라이더 충돌 시 콜라이더 비움
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerExit(Collider other)
    {
        this.other = null;
    }
    #endregion Unity Method


    #region Method

    public void Attack()
    {
        // 충돌된 객체가 없으면 메서드 종료
        if(other == null)
        {
            return;
        }

        // 지금 현재 실행중인 애니메이션이 Attack일 경우에만 실행
        if (animator.GetCurrentAnimatorStateInfo(0).IsName("Attack"))
        {
            // 좀비와 충돌 했을 때만 데미지를 입힌다.
            // 플레이어의 공격 애니메이션이 실행중일 때만 데미지를 입힌다.
            if (other.tag == "Enemy")
            {
                EnemyHealth enemyHealth = other.GetComponent<EnemyHealth>();
                // other.GetComponent<Enemy>().SetDesPos(gameObject.GetComponentInParent<PlayerMove>().gameObject);     // 적은 공격을 받으면 플레이어를 타겟으로 설정
                // 플레이어 공격력만큼 에너미 체력 감소
                enemyHealth.ApplayDamage(attack);
            }
        }
    }

    #endregion Method
}
