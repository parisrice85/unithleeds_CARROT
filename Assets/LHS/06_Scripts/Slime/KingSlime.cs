using UnityEngine;

public class KingSlime : MonoBehaviour
{
    float currentTime;          // 쿨타임 비교용 시간
    public float skillCoolTime; // 스킬 쿨타임

    public GameObject[] skills; // 스킬 배열

    // Update is called once per frame
    void Update()
    {
        // 시간++
        currentTime += Time.deltaTime;
        
        // 시간 > 쿨타임
        if(currentTime > skillCoolTime)
        {
            // 시간 초기화
            currentTime = 0;
            
            // 스킬 호출
            Skill();
        }
    }

    void Skill()
    {
        // 랜덤 스킬
        int skillNum = Random.Range(0, skills.Length);
        
        // 스킬 생성
        var bossSkill = Instantiate(skills[skillNum], transform.position, Quaternion.identity);
        
        // 스킬 오브젝트 파괴
        Destroy(bossSkill, 2f);
    }
}
