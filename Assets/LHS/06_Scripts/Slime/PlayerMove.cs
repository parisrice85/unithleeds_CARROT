using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

public class PlayerMove : MonoBehaviour
{
    #region Variables

    public float speed = 5f;            // 플레이어 속도
    public float jumpHeight = 2f;       // 점프 높이

    // 플레이어와 땅과의 거리, 땅에 있는지 체크하는 용도
    public float groundCheckDistance = 0.3f;

    public LayerMask groundLayerMask;   // Ground 레이어 마스크
    public bool isGrounded = true;      // 플레이어가 땅에 있는지 여부

    private Rigidbody rigidbody;        // 물리력을 담당하는 Rigidbody 컴포넌트
    private Animator animator;          // 플레이어 애니메이터
    private Transform cameraTrans;      // 카메라 트랜스폼
    public ParticleSystem particle;

    // 카메라 이동 처리
    public MainCameraAction mainCameraAction;

    private PlayerHealth playerHealth;  // 플레이어 건강 관리 클래스

    #endregion Variables


    #region Unity Method

    // 초기화
    void Start()
    {
        // 플레이어 객체가 가지고 있는 Rigidbody 컴포넌트와,
        // Animator 컴포넌트를 가져온다.
        animator = GetComponent<Animator>();
        rigidbody = GetComponent<Rigidbody>();

        // 플레이어 건강 관리 클래스 가져오기
        playerHealth = GetComponent<PlayerHealth>();

        // 마우스 표시
        /*Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;*/
    }

    // Update 보다 빠르게 실행
    private void FixedUpdate()
    {
        // 플레이어가 죽었을 경우 이동되지 않도록 설정
        if (playerHealth.isDie == true)
            return;

        // 플레이어 이동
        Move();
    }

    // 매 프레임마다 반복
    void Update()
    {
        // 플레이어가 죽었을 경우 이동되지 않도록 설정
        if (playerHealth.isDie == true)
            return;

        // 플레이어가 땅에 있는지 체크
        CheckGroundStatus();

        // 플레이어 점프
        Jump();

        // 플레이어 공격
        Attack();
    }

    #endregion Unity Method


    #region Method

    /// <summary>
    /// 플레이어 이동
    /// </summary>
    private void Move()
    {
        // 플레이어 이동 속도
        float walkSpeed = speed;

        // 카메라 트랜스폼 받아오기
        cameraTrans = mainCameraAction.cameraArm;

        // 키보드 입력값 받아오기
        Vector2 moveInput = new Vector2(Input.GetAxis("Horizontal"),
                                        Input.GetAxis("Vertical"));

        // 플레이어 이동여부 확인
        // 0이 아닌 값이 들어올 경우 = true,
        // 0이 들어오면 false
        bool isMove = moveInput.magnitude != 0;

        // left shift를 누를시 달리기
        if(Input.GetKey(KeyCode.LeftShift) && isMove)
        {
            animator.SetBool("IsRun", true);        // 달리기 애니메이션 실행
            animator.SetBool("IsWalk", false);      // 걷기 애니메이션 종료

            // 달리기 시 기존 이동속도의 2배로 설정
            walkSpeed = speed * 2;
        }
        // 플레이어 걷기
        else if(isMove == true)
        {
            animator.SetBool("IsRun", false);       // 달리기 애니메이션 종료
            animator.SetBool("IsWalk", true);       // 걷기 애니메이션 실행

            // 기본 이동 속도 적용
            walkSpeed = speed;
        }
        else
        {
            animator.SetBool("IsRun", false);       // 달리기 애니메이션 종료
            animator.SetBool("IsWalk", false);      // 걷기 애니메이션 종료
        }

        // 플레이어가 이동 중일 때 실행
        if(isMove == true)
        {
            // 앞쪽을 바라보는 좌표값
            Vector3 lookForward = new Vector3(cameraTrans.forward.x, 0f, 
                                    cameraTrans.forward.z).normalized;

            // 좌우를 바라보는 좌표값
            Vector3 lookRight = new Vector3(cameraTrans.right.x, 0f,
                                            cameraTrans.right.z).normalized;

            // 플레이어 이동 거리
            Vector3 moveDir = lookForward * moveInput.y +
                                lookRight * moveInput.x;

            // 플레이어 트랜스폼에 위에서 구해진 이동거리 적용
            transform.forward = moveDir;

            // 플레이어 객체가 물리적으로 이동하도록 설정
            rigidbody.MovePosition(transform.position + moveDir * walkSpeed * Time.deltaTime);
        }
    }

    /// <summary>
    /// 플레이어 점프
    /// </summary>
    private void Jump()
    {
        // 땅에 있을 때만 플레이어가 점프하도록 설정
        if(Input.GetKeyDown(KeyCode.Space) && isGrounded)
        {
            Vector3 jumpVelocity = Vector3.up * 
                        Mathf.Sqrt(jumpHeight * -2f * Physics.gravity.y);

            // 위에서 구해진 점프 속도를 플레이어 물리력에 적용
            rigidbody.AddForce(jumpVelocity, ForceMode.VelocityChange);

            // 점프 애니메이션 실행
            animator.SetTrigger("IsJump");
        }
    }

    /// <summary>
    /// 플레이어 공격
    /// </summary>
    private void Attack()
    {
        // 마우스 왼쪽 버튼 누를 시 공격 모션 실행
        if (Input.GetButtonDown("Fire1") && isGrounded)
        {
            animator.SetTrigger("IsAttack");
            particle.Play();
        }
    }

    /// <summary>
    /// 플레이어가 땅에 있는지 체크하는 메서드
    /// </summary>
    private void CheckGroundStatus()
    {
        // 레이저를 쏴서 충돌정보 저장
        RaycastHit hitInfo;

        // 플레이어 위치에서 땅으로 레이저를 쏴서 Ground라는 레이어 마스크를
        // 가진 충돌 객체가 있으면 isGround를 true로 설정하고
        // 공중에 뜬 상태라면 isGround를 false로 설정한다.
        if (Physics.Raycast(transform.position + (Vector3.up * 0.1f),
            Vector3.down, out hitInfo, groundCheckDistance, groundLayerMask))
        {
            isGrounded = true;
            animator.SetBool("IsGround", true);
        }
        else
        {
            isGrounded = false;
            animator.SetBool("IsGround", false);
        }
        Debug.DrawRay(transform.position + (Vector3.up * 0.1f),
            Vector3.down, Color.red, groundCheckDistance);
    }

    #endregion Method
}
