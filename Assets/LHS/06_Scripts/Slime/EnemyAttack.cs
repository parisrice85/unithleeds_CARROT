using UnityEngine;

public class EnemyAttack : MonoBehaviour
{
    #region Variables

    private Enemy enemy;                       // 에너미 기본 클래스
    private PlayerScript playerScript;          // 플레이어 건강 관리 클래스
    public int attack = 1;                     // 에너미 공격력
    public Animator animator;                   // 에너미 애니메이터

    public float attackDelay;

    #endregion Variables


    #region Unity Method

    // 에너미의 공격 포인트와 충돌했을 때
    // 플레이어에게 데미지 입히기
    private void OnTriggerStay(Collider other)
    {
        attackDelay += Time.deltaTime;
        // 충돌한 객체의 태그가 플레이어아 아닐 경우 메서드 종료
        if (other.tag != "player") return;

        // 플레이어 태그일 경우 PlayerHealth 컴포넌트를 가져와서
        // 데미지 처리
        if(other.GetComponent<PlayerScript>()._playerLive)
            PlayerAttack(other.GetComponent<PlayerScript>());
    }


    #endregion Unity Method


    #region Method

    /// <summary>
    /// 플레이어를 공격하는 메서드
    /// </summary>
    /// <param name="playerHealth">공격할 플레이어</param>
    public void PlayerAttack(PlayerScript script)
    {
        // 지금 현재 실행중인 애니메이션이 Attack일 경우에만 실행
        if (animator.GetCurrentAnimatorStateInfo(0).IsName("Attack"))
        {
            // 에너미의 공격력만큼 플레이어 체력 감소
            script.Damaged(attack);


            if (!script._playerLive)
            {
                // 플레이어 사망 후 에너미 애니메이션 기본으로 변경
                animator.SetBool("IsRun", false);
                animator.SetBool("IsAttack", false);

                Debug.Log("Enemy Attack 클래스 / 플레이어 사망후 Idle 변경");
            }
        }
    }

    #endregion Method
}
