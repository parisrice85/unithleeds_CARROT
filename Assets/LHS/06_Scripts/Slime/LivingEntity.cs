using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 생명체로서 동작할 개임 오브텍트들을 위한 뼈대를 제공
/// 체력 등 초기화 작업, 데미지 처리, 사망 처리 제공
/// </summary>
public class LivingEntity : MonoBehaviour
{
    #region Variables

    public int startHealth = 100;           // 시작 체력
    public int currentHealth { get; protected set; }      // 현재 체력
    public bool isDie { get; protected set; }            // 생명체 죽은 여부

    private const float minTimeBetDamaged = 1f;       // 최소 데미지 타임
    private float lastDamagedTIme;                      // 마지막 데미지를 받은 시간
    #endregion Variables


    #region Unity Method



    #endregion Unity Method


    /// <summary>
    /// 색명체 무적 여부 판별
    /// </summary>
#region Method

    protected bool Isinvulnerable
    {
        get
        {
            // 무적 시간인지 확인
            // 마지막 데미지 받은 시간에서 데미지 인정 최소 시간을 더한 시간보다 크면
            // 공격 가능한 상태
            if (lastDamagedTIme + minTimeBetDamaged <= Time.time)
            {
                return false;
            }

            // 무적 상태
            return true;
        }
    }

    /// <summary>
    /// 생명체 초기화
    /// </summary>
    protected virtual void Init()
    {
        // 현재 체력 = 시작 체력으로 초기화
        currentHealth = startHealth;

        // 사망하지 않은 상태로 시작
        isDie = false;
    }

    /// <summary>
    /// 데미지 처리 메서드
    /// </summary>
    /// <returns></returns>
    public virtual bool ApplayDamage(int damage)
    {
        // 생명체가 무적상태이거나 생명체가 죽었을 경우 데미지 처리 불가
        if(Isinvulnerable == true || isDie == true)
        {
            return false;
        }

        // 마지막 데미지 시간을 현재 시간으로 설정
        lastDamagedTIme = Time.time;

        // 데미지 만큼 체력 감소
        currentHealth -= damage;

        // 체력이 0 이하일 경우 사망 처리 실행
        if(currentHealth <= 0 )
        {
            currentHealth = 0;
            Die();
        }

        return true;
    }

    /// <summary>
    /// 체력을 회복하는 메서드
    /// </summary>
    /// <param name="newHealth"></param>
    public virtual void RestoreHealth(int newHealth)
    {
        // 생명체가 죽었다면 메서드 종료
        if (isDie)
            return;

        // 체력을 추가
        currentHealth += newHealth;

        // 추가된 현재 체력이 시작체력보다 크다면
        // 현재 체력을 시작 체력값을 설정
        if(currentHealth > startHealth)
        {
            currentHealth = startHealth;
        }

    }

    /// <summary>
    /// 사망처리
    /// </summary>
    public virtual void Die()
    {
        if(currentHealth <= 0)
            isDie = true;
    }
}
#endregion Method
