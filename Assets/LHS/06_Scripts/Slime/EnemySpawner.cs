using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    #region Variables

    public Transform[] EnemySpawnPoints;             // 에너미 생성 위치 객체들
    private int randPos;                            // 랜덤 생성 위치 번호
    public GameObject[] enemys;                      // 랜덤 생성할 에너미 객체들
    private int randEnemy;                           // 랜덤 생성될 에너미 번호
    public float radius = 3f;                      // 에너미 위치에서 랜덤 생성될 반지름 길이

    #endregion Variables


    #region Unity Method


    // 초기화
    void Start()
    {
        // 5초에 한번씩 아이템 생성 메서드 실행
        // InvokeRepeating("SpawnEnemy", 5, 1);
    }

    #endregion Unity Method


    #region Method

    /// <summary>
    /// 아이템 생성 메서드
    /// </summary>
    public void SpawnEnemy(Transform transform)
    {
        // 아이템 생성 위치 개수 만큼 반복
        for (int i = 0; i < EnemySpawnPoints.Length; i++)
        {
            // 랜덤 아이템 지정
            randEnemy = Random.Range(0, enemys.Length);

            // 랜덤 x좌표 지정
            float radomX = Random.Range(-radius, radius);

            // 랜덤 z좌표 지정
            float radomZ = Random.Range(-radius, radius);

            // 랜덤으로 생성될 아이템의 위치 지정
            Vector3 enemyPos = transform.position + new Vector3(radomX, 0, radomZ);

            Vector3 dir = (gameObject.transform.position - transform.position).normalized;

            // 랜덤 아이템 객체 생성
            // Instantiate(생성할 오브젝트, 생성할 위치, 회전 값)
            GameObject slime = Instantiate(enemys[randEnemy], enemyPos, Quaternion.Euler(dir));
        }
    }

    #endregion Method
}
