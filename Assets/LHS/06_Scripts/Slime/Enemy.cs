using UnityEngine;
using UnityEngine.AI;

public class Enemy : MonoBehaviour
{
    #region Variables
    public GameObject target;               // 좀비가 따라다닐 타겟
    public float fieldOfView = 140f;         // 좀비 시야각
    public float viewDistance = 10f;        // 좀비 시야거리
    public float attackDistance = 1f;       // 좀비 공격범위

    public Transform eye;                   // 좀비 눈 트렌스폼
    public float speed = 10f;               // 좀비 속도
    private Animator animator;              // 좀비 애니메이터
    private NavMeshAgent agent;             // 좀비 네브메쉬 에이전트 컴포넌트
    public Vector3 randPos;                 // 좀비가 뛰어다닐 랜덤 위치

    public LayerMask targetLayer;           // 타겟 레이어를 Player로 설정

    private Rigidbody rigid;

    #endregion Variables


    #region Unity Method

#if UNITY_EDITOR

    /// <summary>
    /// 좀비 시야각 그리기
    /// </summary>
    /*private void OnDrawGizmosSelected()
    {
        // 시야각 회전값
        Quaternion leftRayRotation = Quaternion.AngleAxis(-fieldOfView * 0.5f, Vector3.up);

        // 시야각 거리값
        Vector3 leftRotDirection = leftRayRotation * transform.forward;

        // 시야각 컬러
        Handles.color = Color.red;
        Handles.DrawSolidArc(eye.position, Vector3.up,
            leftRotDirection, fieldOfView, viewDistance);
    }*/

#endif



    // 초기화
    void Start()
    {
        // 애니메이터 컴포넌트 가져오기
        animator = GetComponent<Animator>();

        // 좀비 달리기 실행
        //animator.SetBool("IsRun", true);

        // 네브메쉬 컴포넌트 가져오기
        // agent = GetComponent<NavMeshAgent>();
        
        // 리지드 컴포넌트 가져오기
        rigid = GetComponent<Rigidbody>();

        /*// 랜덤 위치 설정
        RandPosSetting();

        // 좀비 초기 타겟은 랜덤 위치로 설정
        agent.SetDestination(randPos);*/
        
        // 플레이어 찾기
        target = GameObject.FindWithTag("player");
    }

    /*public void SetDesPos(GameObject pos)
    {
        agent.SetDestination(pos.transform.position);
        target = pos.gameObject;
    }*/

    // 매 프레임마다 반복
    void FixedUpdate()
    {
        if (gameObject.layer == 8)
        {
            return;
        }

        if (target != null)
        {
            // 슬라임 방향 설절
            Vector3 dir = (target.transform.position - transform.position).normalized;
            
            // 슬라임 이동
            rigid.position += dir * speed * Time.deltaTime;
            //transform.forward = (_target.transform.position - transform.position).normalized;
            
            // 플레이어 바라보기
            transform.LookAt(target.transform);
            // transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(dir), Time.deltaTime * 3f);
            
            // 플레이어가 공격 범위 안에 들어오면
            if ((target.transform.position - transform.position).magnitude < attackDistance)
            {
                // 공격 애니메이션 실행
                animator.SetBool("IsRun", false);
                animator.SetBool("IsAttack", true);

                // agent 멈춤
                // agent.isStopped = true;
                
                // 공격 시 속도 값 0
                rigid.velocity = Vector3.zero;

                // 때리면 뒤로가기(공격공간 확보)
                Vector3 backDir = (target.transform.position - transform.position).normalized;
                rigid.MovePosition((transform.position -= backDir) * Time.deltaTime);
            }
            else
            {
                // 공격 범위 밖이라면 공격 애니메이션 false
                animator.SetBool("IsRun", true);
                animator.SetBool("IsAttack", false);
            }
        }

        /*Collider[] colliders = Physics.OverlapSphere(eye.position, viewDistance, targetLayer);

        *//*for (int i = 0; i < colliders.Length; i++)
        {            
            if (colliders[i].gameObject.name == "Player")
            {
                if (colliders[i].gameObject.GetComponent<PlayerHealth>().isDie)
                {
                    target = target2;
                    return;
                }
            }
        }*//*

        // 타겟과 좀비 사이의 거리
        float distance;

        // 타겟이 없을 경우 랜덤 좌표와 거리 구하기
        if(target == null)
        {
            *//*distance = Vector3.Distance(randPos, transform.position);

            if(distance < 1f)
            {
                // 랜덤 위치 설정
                RandPosSetting();

                // 새로 지정된 랜덤 위치로 이동
                agent.SetDestination(randPos);
            }*//*
            return;
        }

        // 타겟(플레이어)가 있을 경우 타겟과의 거리 구하기
        else
        {
            distance = Vector3.Distance(target.transform.position, transform.position);
        }


        foreach(Collider col in colliders)
        {
            // 충돌 타겟 (플레이어)가 없다면 반복문 종료
            if (IsTargetFind(col.gameObject) == false)
            {
                break;
            }

            *//*PlayerInfo p = col.GetComponent<PlayerInfo>();

            if(p == null)
            {
                Debug.Log("플레이어인포없음");
                break;
            }*//*

            // 무기가 아닐 경우만 실행
            if(col.name != "Weapon")
            {
                Debug.Log(col.name);
                // 플레이어 건강 관리 컴포넌트 가져오기
                PlayerScript pS = col.GetComponent<PlayerScript>();

                // 플레이어가 죽었다면 에너미 정지
                if (!pS._playerLive)
                {
                    animator.SetBool("IsRun", false);
                    animator.SetBool("IsAttack", false);

                    // agent 멈춤
                    

                    break;
                }
                // 플레이어가 살아있다면 타겟으로 설정
                else
                {
                    target = col.gameObject;

                    // 플레이어를 바라보도록 설정
                    transform.LookAt(target.transform.position);
                    break;
                }
            }            
        }


        // 좁비와 타겟 사이의 거리가 공격 범위에 들어왔다면
        // 달리기를 멈추고 플레이어를 공격
        if (distance <= attackDistance)
        {
            animator.SetBool("IsRun", false);
            animator.SetBool("IsAttack",true);

            // agent 멈춤
            // agent.isStopped = true;

            rigid.velocity = Vector3.zero;

            // 때리면 뒤로가기(공격공간 확보)
            Vector3 backDir = (target.transform.position - transform.position).normalized;
            transform.Translate((transform.position -= backDir) * Time.deltaTime);
        }
        // 타겟(플레이어)가 공격범위 밖이라면 타겟(플레이어)를 쫓는다
        else
        {
            rigid.MovePosition(target.transform.position * speed * Time.deltaTime);

            animator.SetBool("IsRun", true);
            animator.SetBool("IsAttack", false);

            // agent 움직이도록 설정
            // agent.isStopped = false;
        }

        // 타겟이 있을 경우 타겟으로 쫓도록 설정
        if (target != null)
        {
            // agent.SetDestination(target.transform.position);
            rigid.MovePosition(target.transform.position * speed * Time.deltaTime);
        }*/
    }


    #endregion Unity Method


    #region Method
    
    // 뽕망치 피격 이펙트
    public void HitbyPico(Vector3 explosionPos)
    {
        
        Vector3 reactVec = transform.position - explosionPos;
        reactVec = reactVec.normalized;
        reactVec += Vector3.up;
        rigid.AddForce(reactVec * 3, ForceMode.Impulse); 
    }
    /// <summary>
    /// 랜덤 위치 설정 메서드
    /// </summary>
    /*private void RandPosSetting()
    {
        // 랜덤 x좌표
        float randX = Random.Range(-2f, 2f);

        // 랜덤 z좌표
        float randZ = Random.Range(-2f, 2f);

        // 랜덤 위치 설정
        randPos = new Vector3(randX, 0, randZ);
    }*/

    /// <summary>
    /// 타겟을 찾았는지 판별하는 메서드
    /// </summary>
    /// <param name="target"></param>
    /// <returns></returns>
    /*private bool IsTargetFind(GameObject target)
    {
        // 충돌 객체 저장
        RaycastHit hit;

        // 좀비와 타겟(플레이어) 사이의 거리
        Vector3 direction = target.transform.position - eye.position;

        // 타겟이 시야각 보다 밖에 있을 경우
        if(Vector3.Angle(direction, eye.forward) > fieldOfView * 0.5f)
        {
            return false;
        }

        // 좀비 시야각에 플레이어가 충돌했는지 체크
        if(Physics.Raycast(eye.position, direction, out hit, viewDistance))
        {
            // 충돌 된 객체가 플레이어 태그가 있을 경우 실행
            if(hit.transform.gameObject.tag == "player")
            {
                return true;
            }            
        }

        return false;
    }*/

    #endregion Method
}
