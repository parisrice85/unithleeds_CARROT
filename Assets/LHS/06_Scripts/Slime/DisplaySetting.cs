using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisplaySetting1 : MonoBehaviour
{
    #region Variables

    private FullScreenMode screenMode;      // 풀 스크린 모드
    public Dropdown resolutionDropdown;     // 드롭다운 UI 오브젝트
    public Toggle fullScreenBtn;            // 토글 UI 오브젝트

    // 해상도 리스트
    private List<Resolution> resolutions = new List<Resolution>();
    private int resolutionNum;              // 선택한 해상도 번호

    #endregion Variables


    #region Unity Method


    // 초기화
    void Start()
    {
        // UI 초기화
        InitUI();
    }

    #endregion Unity Method


    #region Method

    /// <summary>
    /// UI 초기화
    /// </summary>
    private void InitUI()
    {
        // 해상도 사이즈가 담긴 리스트 길이만큼 반복
        for (int i = 0; i < Screen.resolutions.Length; i++)
        {
            // 해상도 프레임이 60인 해상도 사이즈만 가져온다.
            if ((Screen.resolutions[i].refreshRate == 60))
            {
                resolutions.Add(Screen.resolutions[i]);
            }
        }

        // 드롭다운 하위의 옵션 데이터를 초기화
        resolutionDropdown.options.Clear();

        // 옵션 번호
        int optionNum = 0;

        // 위에서 정의한 Resolution 리스트 길이만큼 반복
        foreach(Resolution resolution in resolutions)
        {
            // 드롭다운 하위 옵션 변수를 가져옴
            Dropdown.OptionData option = new Dropdown.OptionData();

            // 해상도 가로 x 세로 프레임
            option.text = resolution.width + " x " + resolution.height
                + " " + resolution.refreshRate + "hz";

            // 드롭다운 옵션에 위에 작성된 데이터 추가
            resolutionDropdown.options.Add(option);

            // 현재 해상도 사이즈가 드롭다운 콤보박스 가장 첫번째
            // 값으로 지정됨
            if(resolution.width == Screen.width &&
               resolution.height == Screen.height)
            {
                resolutionDropdown.value = optionNum;
            }

            // 옵션 번호 +1
            optionNum++;
        }

        // 드롭다운 데이터 새로고침 (정상적 적용을 위해서)
        resolutionDropdown.RefreshShownValue();

        // 삼항연산자
        // 만약 실행된 프로그램이 풀 스크린 모드로 잡혀있다면 true
        // 창모드라면 false로 설정됨
        fullScreenBtn.isOn = Screen.fullScreenMode.Equals(
            FullScreenMode.FullScreenWindow) ? true : false;
    }

    /// <summary>
    /// 드롭다운 해상도 변경
    /// </summary>
    /// <param name="_resolutionNum">선택한 해상도 번호</param>
    public void DropboxOptionChange()
    {
        resolutionNum = resolutionDropdown.value;
    }

    /// <summary>
    /// 풀 스크린 여부를 체크하는 토글키 버튼을 누를 시 실행됨
    /// </summary>
    public void FullScreenBtn()
    {
        // 풀 스크린이 선택되었다면 전체화면으로 전환
        screenMode = fullScreenBtn.isOn ? FullScreenMode.FullScreenWindow :
            FullScreenMode.Windowed;
    }

    /// <summary>
    /// 설정버튼을 클릭하면 선택한 해상도와 전체화면 여부가 프로그램에 적용됨
    /// </summary>
    public void DisPlaySettingBtnClick()
    {
        // 선택된 해상도 가로, 세로 사이즈와 전체화면 여부가 스크린에 적용됨
        Screen.SetResolution(resolutions[resolutionNum].width,
            resolutions[resolutionNum].height, screenMode);

        Debug.Log(resolutions[resolutionNum].width + " x " + resolutions[resolutionNum].height + screenMode);
    }

    #endregion Method
}
