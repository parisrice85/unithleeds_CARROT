using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletPJY : MonoBehaviour
{
    public float speed = 8f;
    private Rigidbody bulletRigidbody;
    private Transform player;

    // Start is called before the first frame update
    void Start()
    {
        player = FindAnyObjectByType<PlayerScript>().transform;
        bulletRigidbody = GetComponent<Rigidbody>();
        bulletRigidbody.velocity = transform.forward * speed;
        //bulletRigidbody.AddForceAtPosition((transform.forward + transform.up).normalized * speed,  player.up * 3f);
        /*Vector3 firstPos = transform.position;
        Vector3 thirdPos = firstPos + player.position;
        Vector3 secondPos = (firstPos + thirdPos) / 2;
        transform.DOPath(new[] { secondPos, firstPos + Vector3.up, secondPos + Vector3.left * 2, thirdPos, secondPos + Vector3.right * 2, thirdPos + Vector3.up }, 1f, PathType.CubicBezier).SetEase(Ease.Unset);*/

        
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "player")
        {
            PlayerScript playerScript = other.GetComponent<PlayerScript>();

            Debug.Log("������");

            if (playerScript != null)
            {
                playerScript.Damaged(1);
            }
        }
        else if (other.tag == "Planet")
        {
            bulletRigidbody.AddExplosionForce(10f, transform.position, 5f);
            Destroy(gameObject);
            Debug.Log("������");

        }
        else if(other.tag == "enemy")
        {
            Debug.Log("������");

        }
        Debug.Log("������");

    }
}