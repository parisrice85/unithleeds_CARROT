using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bullet : MonoBehaviour
{
    
    public float speed = 8f;            // 탄알 이동 속력
    private Rigidbody bulletRigidbody;  // 이동에 사용할 리지드바디 컴포넌트

    // Start is called before the first frame update
    void Start()
    {
        // 게임 오브젝트에서 rigidbody 컴포넌트를 찾아 bulletrigidbody에 할당
        bulletRigidbody = GetComponent<Rigidbody>();

        // 리지드바디의 속도 = 앞쪽 방향 * 속력 
        bulletRigidbody.velocity = transform.forward * speed;

        Destroy(gameObject, 3f);
        Debug.Log("출발");
    } 
        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Player"))
            {
                PlayerScript playerScript = other.GetComponent<PlayerScript>();
                if (playerScript != null)
                {
                    playerScript.Damaged(1);
                }
                Destroy(gameObject);
            }
        }
    }



